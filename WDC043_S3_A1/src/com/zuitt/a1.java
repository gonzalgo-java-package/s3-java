package com.zuitt;

import java.util.InputMismatchException;
import java.util.Scanner;

public class a1 {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        //while loop


        int num = 0;
        try{
            System.out.println("Input an integer whose factorial will be computed:");
            num = input.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("Input is not a number");
        } finally {
            int answer = 1;
            int counter = 1;

            if(num == 0){
                System.out.println("The factorial of " + num + " is 1");
            } else if (num < 0) {
                System.out.println("Undefined");
            } else {
                while(counter <= num){
                    answer = answer * counter;
                    counter++;
                }
                System.out.println("The factorial of " + num + " is " + answer);
            }
        }


        //for loop
        int num2 = 0;

        try{
            System.out.println("Input an integer whose factorial will be computed:");
            num2 = input.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("Input is not a number");
        } catch (Exception e) {
            System.out.println("Invalid Input");
        } finally {
            if(num2 != 0){
                int c,a = 1;
                for(c = 1; c <= num2; c++){
                    a = a * c;
                }

                System.out.println("The factorial of " + num + " is " + a);
            }
        }

    }
}
