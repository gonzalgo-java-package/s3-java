package com.zuitt.batch193;

import java.util.Scanner;

public class UserInput {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        System.out.println("Input a number");
        int num = input.nextInt();

        System.out.println("The number you entered is: " + num);

        //run-time errors

        //compile-time errors

        // exception handling


    }
}
